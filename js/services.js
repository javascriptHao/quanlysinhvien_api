// LINK DEFAULT API
const BASE_URL = "https://64819cf729fa1c5c5031bf0a.mockapi.io/sinhvien";

// AXIOS API
let sinhVienServ = {
  getList: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  create: (sv) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: sv,
    });
  },
  delete: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
  getById: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },
  update: (id, sv) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: sv,
    });
  },
};
