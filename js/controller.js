// RENDER DSSV
function renderDSSV(dssv) {
  let contentHTML = "";
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    var content = `<tr>
  <td>${sv.id}</td>
  <td>${sv.name}</td>
  <td>${sv.email}</td>
  <td>${sv.tinhDTB()}</td>
  <td>
  <button onclick="suaSinhVien(${sv.id})" class="btn btn-warning">Sửa</button>
  <button onclick="xoaSinhVien('${sv.id}')" class="btn btn-danger">Xóa</button>
  </td>
  </tr>`;
    contentHTML += content;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

// LẤY THÔNG TIN TỪ FORM
function layThongTinTuForm() {
  let id = document.getElementById("txtMaSV").value;
  let name = document.getElementById("txtTenSV").value;
  let email = document.getElementById("txtEmail").value;
  let password = document.getElementById("txtPass").value;
  let toan = Number(document.getElementById("txtDiemToan").value);
  let ly = Number(document.getElementById("txtDiemLy").value);
  let hoa = Number(document.getElementById("txtDiemHoa").value);
  return { id, name, email, password, toan, ly, hoa };
}

// SHOW THÔNG TIN LÊN FORM
function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.id;
  document.getElementById("txtTenSV").value = sv.name;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.password;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

// BẬT LOADING
function batLoading() {
  document.getElementById("spinner").style.display = "flex";
}

// TẮT LOADING
function tatLoading() {
  document.getElementById("spinner").style.display = "none";
}
