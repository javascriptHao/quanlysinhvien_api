// CLASS SINH VIEN
class SinhVien {
  constructor(id, name, email, password, toan, ly, hoa) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
    this.toan = toan;
    this.ly = ly;
    this.hoa = hoa;
  }
  tinhDTB = () => {
    return Math.round((this.toan * 1 + this.ly * 1 + this.hoa * 1) / 3);
  };
}
