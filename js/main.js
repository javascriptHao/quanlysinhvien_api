// LẤY DSSV TỪ SERVER VÀ RENDER RA LAYOUT
function fetchSinhVienList() {
  batLoading();
  sinhVienServ
    .getList()
    .then(function (res) {
      dssv = res.data.reverse().map((item) => {
        return new SinhVien(
          item.id,
          item.name,
          item.email,
          item.password,
          item.toan,
          item.ly,
          item.hoa
        );
      });
      renderDSSV(dssv);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
}
fetchSinhVienList();
document.getElementById("btnUpdate").disabled = true;

// XÓA GIÁ TRỊ FORM
function resetForm() {
  document.getElementById("formQLSV").reset();
  document.getElementById("btnUpdate").disabled = true;
  document.getElementById("btnAdd").disabled = false;
}

// KIỂM TRA VALIDATION
function checkValidate(sv) {
  let isValid = kiemTraDoDai(
    1,
    50,
    "spanTenSV",
    "Tên không được để trống",
    sv.name
  );
  isValid &= kiemTraDoDai(
    1,
    50,
    "spanMatKhau",
    "Mật khẩu không được để trống",
    sv.password
  );
  isValid &= kiemTraEmail(sv.email);
  return isValid;
}

// THÊM SINH VIÊN
function themSinhVien() {
  var newSV = layThongTinTuForm();
  if (checkValidate(newSV)) {
    batLoading();
    sinhVienServ
      .create(newSV)
      .then(function (res) {
        fetchSinhVienList();
      })
      .catch(function (err) {
        tatLoading();
      });
    resetForm();
  }
}

// SỬA SINH VIÊN
function suaSinhVien(id) {
  batLoading();
  sinhVienServ
    .getById(id)
    .then(function (res) {
      showThongTinLenForm(res.data);
      tatLoading();
      document.getElementById("btnUpdate").disabled = false;
      document.getElementById("btnAdd").disabled = true;
    })
    .catch(function (err) {
      tatLoading();
    });
}

// CẬP NHẬT SINH VIÊN
function capNhatSinhVien() {
  var sv = layThongTinTuForm();
  if (checkValidate(sv)) {
    batLoading();
    sinhVienServ
      .update(sv.id, sv)
      .then(function (res) {
        fetchSinhVienList();
      })
      .catch(function (err) {
        tatLoading();
      });
    resetForm();
    document.getElementById("btnUpdate").disabled = true;
    document.getElementById("btnAdd").disabled = false;
  }
}

// XÓA SINH VIÊN
function xoaSinhVien(id) {
  batLoading();
  sinhVienServ
    .delete(id)
    .then(function (res) {
      fetchSinhVienList();
    })
    .catch(function (err) {
      tatLoading();
    });
}
