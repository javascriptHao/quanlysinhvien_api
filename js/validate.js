// SHOW MESSAGE
function showMessage(idSpan, message) {
  document.getElementById(idSpan).innerText = message;
}

// KIỂM TRA ĐỘ DÀI
function kiemTraDoDai(min, max, idSpan, message, value) {
  var length = value.trim().length;
  if (length >= min && length <= max) {
    showMessage(idSpan, "");
    return true;
  } else {
    showMessage(idSpan, message);
    return false;
  }
}

// KIỂM TRA EMAIL
function kiemTraEmail(email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    showMessage("spanEmailSV", "");
    return true;
  } else {
    showMessage("spanEmailSV", "Email không hợp lệ");
    return false;
  }
}
